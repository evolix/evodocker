#!/bin/sh
set -e

count_tasks() {
    getent hosts tasks."$1" |wc -l
}

# Dirty hack to ensure that "getent hosts tasks.XXXX" will list us
sleep 3


## STARTING REDIS ##
if [ "$1" = "redis-server" ]; then
  echo "[DEBUG] PREPARING to start redis-server..."

  # Check how many redis replicas are currently running...
  if [ "$(count_tasks redis)" -gt 1 ]; then
    # Other instances are running, we shall ask to one sentine who is master
    echo "[DEBUG] Already one redis (or more) instance running, we'll ask the master IP to a sentinel and set ourself as slave..."

    # TODO: sanity check : Manage the case where there's no sentinel ?

    # Wait for a sentinel to be alive
    until [ "$(redis-cli -h "$REDIS_SENTINEL_IP" -p "$REDIS_SENTINEL_PORT" ping)" = "PONG" ]; do
      echo "$REDIS_SENTINEL_IP is unavailable - sleeping"
      sleep 1
    done

    # Get current master IP
    master_info=$(redis-cli -h "$REDIS_SENTINEL_IP" -p "$REDIS_SENTINEL_PORT" sentinel get-master-addr-by-name "$REDIS_MASTER_NAME")

    # Ensure we have something from sentinel
    until [ "$master_info" ]; do
      echo "$REDIS_MASTER_NAME not found - sleeping"
      sleep 1
      master_info=$(redis-cli -h "$REDIS_SENTINEL_IP" -p "$REDIS_SENTINEL_PORT" sentinel get-master-addr-by-name "$REDIS_MASTER_NAME")
    done

    master_ip=$(echo $master_info | awk '{print $1}')
    master_port=$(echo $master_info | awk '{print $2}')

    echo "[DEBUG] STARTING redis as a SLAVE of $master_ip $master_port"
    redis-server --slaveof $master_ip $master_port

  else
    # Only one redis - So we may be master? - TODO: Add sanity checks ?
    # Like : Is there any sentinels ? What are they doing ?...
    echo "[DEBUG] No other redis are running.... Starting redis as MASTER !!"
    exec "$@"

  fi


## STARTING SENTINEL ##
elif [ "$1" = "redis-sentinel" ]; then
  echo "[DEBUG] PREPARING sentinel"

  # Update config with ENV
  sed -i "s/{{ SENTINEL_QUORUM }}/$SENTINEL_QUORUM/g" /usr/local/etc/redis/sentinel.conf
  sed -i "s/{{ SENTINEL_DOWN_AFTER }}/$SENTINEL_DOWN_AFTER/g" /usr/local/etc/redis/sentinel.conf
  sed -i "s/{{ SENTINEL_FAILOVER }}/$SENTINEL_FAILOVER/g" /usr/local/etc/redis/sentinel.conf
  sed -i "s/{{ REDIS_MASTER_NAME }}/$REDIS_MASTER_NAME/g" /usr/local/etc/redis/sentinel.conf

  # Check how many sentinels replicas are currently running...
  if [ "$(count_tasks redissentinel)" -gt 1 ]; then
    echo "[DEBUG] Already one (or more) sentinel instance running, we'll ask the master IP to a sentinel and set it as the master"

    # Fetch sentiten
    sentinel_ips=$(getent hosts tasks.redissentinel | awk '{print $1}');

    for ip in $sentinel_ips; do
      REDIS_IP=$(redis-cli -h "$ip" -p 26379 sentinel get-master-addr-by-name "$REDIS_MASTER_NAME" | head -1)
      echo "[DEBUG] According to sentinel at $ip - Master_info is : $REDIS_IP"

      if [ "$REDIS_IP" ]; then
        break
      fi
    done


    sed -i "s/{{ REDIS_IP }}/$REDIS_IP/g" /usr/local/etc/redis/sentinel.conf

    echo "[DEBUG] Starting sentinel with redismaster as $REDIS_IP"
    redis-server /usr/local/etc/redis/sentinel.conf --sentinel

  else
    # No other sentinels around.
    echo "[DEBUG] No other sentinel running...."

    # Is there redis instance running ?
    if [ "$(count_tasks redis)" -gt 1 ]; then
      # More than 1 redis running... Not so fun case to manage....
      echo "[DEBUG] More than 1 redis running - Giving up...."

    elif [ "$(count_tasks redis)" -eq 1 ]; then

      echo "[DEBUG] Only one redis is runnig. Let's say its a master!"
      REDIS_IP=$(getent hosts tasks.redis | awk '{print $1}');

      sed -i "s/{{ REDIS_IP }}/$REDIS_IP/g" /usr/local/etc/redis/sentinel.conf
      redis-server /usr/local/etc/redis/sentinel.conf --sentinel

    else
      echo "[DEBUG] No redis running... - Giving up...."
    fi;
  fi;
fi;
